///////////////////////////////////////////////////////////
//  Buyer.cs
//  Implementation of the Class Buyer
//  Generated by Enterprise Architect
//  Created on:      31-May-2020 8:05:50 PM
//  Original author: Predrag
///////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;



public class Buyer : User {

	private int numberOfPoints;
	private List<Ticket> reservedTickets;
    private UserType type;
    private bool isSuspicious;

	public Buyer(DateTime birthDate, Gender gender, string lastName, string name, string password, string username,UserType type)
        : base(birthDate, gender, lastName, name, password, username)
    {
        numberOfPoints = 0;
        Type = type;
        RoleName = "BUYER";
        IsSuspicious = false;
    }

    public Buyer(DateTime birthDate, Gender gender, string lastName, string name, string password, string username, int ID, bool isDeleted, int points, UserType type, bool isSuspicious)
        : base(birthDate, gender, lastName, name, password, username, ID, isDeleted)
    {
        this.numberOfPoints = points;
        Type = type;
        RoleName = "BUYER";
        IsSuspicious = isSuspicious;
    }

    ~Buyer(){

	}

	public override bool isAdmin(){

		return false;
	}

	public override bool isBuyer(){

		return true;
	}

	public override bool isSeller(){

		return false;
	}

	public int NumberOfPoints{
		get{
			return numberOfPoints;
		}
		set{
			numberOfPoints = value;
		}
	}


    public double CalculateDiscountedPrice(double price)
    {
        return price - (price * type.Discount) / 100;
    }

    public List<Ticket> ReservedTickets { get => reservedTickets; set => reservedTickets = value; }
    public UserType Type { get => type; set => type = value; }
    public bool IsSuspicious { get => isSuspicious; set => isSuspicious = value; }

    public override string Serialize()
    {
        return base.Serialize() + string.Format("|{0}|{1}|{2}", numberOfPoints, Type.TypeName, IsSuspicious);
    }

}//end Buyer