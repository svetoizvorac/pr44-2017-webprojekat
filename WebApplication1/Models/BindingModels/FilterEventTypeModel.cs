﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.BindingModels
{
    public class FilterEventTypeModel
    {
        [Required]
        public List<int> Events { get; set; }
        [Required]
        public EventType EventTypeFilter { get; set; }
    }
}