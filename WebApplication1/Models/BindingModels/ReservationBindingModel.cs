﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.BindingModels
{
    public class ReservationBindingModel
    {
        [Required]
        public int CurrentUser { get; set; }
        [Required]
        public int VipTickets { get; set; }
        [Required]
        public int RegularTickets { get; set; }
        [Required]
        public int FanPitTickets { get; set; }
        [Required]
        public int CurrentEvent { get; set; }
    }
}