﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.BindingModels
{
    public class HomeSearchModel
    {
        public string EventName { get; set; }
        public string EventStreet { get; set; }
        public string EventCity { get; set; }
        public int DayFrom { get; set; }
        public int MonthFrom { get; set; }
        public int YearFrom { get; set; }
        public int DayTo { get; set; }
        public int MonthTo { get; set; }
        public int YearTo { get; set; }
        public double PriceFrom { get; set; }
        public double PriceTo { get; set; }
    }
}