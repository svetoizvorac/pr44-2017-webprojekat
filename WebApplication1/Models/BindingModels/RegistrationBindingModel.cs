﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.BindingModels
{
    public class RegistrationBindingModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Lastname { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public Gender Gender { get; set; }
        [Required]
        public bool IsSeller { get; set; }
        [Required]
        public int Birthday { get; set; }
        [Required]
        public int Birthmonth{ get; set; }
        [Required]
        public int Birthyear { get; set; }

    }
}