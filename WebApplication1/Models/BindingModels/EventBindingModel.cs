﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.BindingModels
{
    public class EventBindingModel
    {
        [Required]
        [StringLength(35, MinimumLength = 5)]
        public string EventName { get; set; }
        [Required]
        public EventType EventType { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int SeatsNumber { get; set; }
        [Required]
        [Range(0, int.MaxValue)]
        public int RegularTicketsNumber { get; set; }
        [Required]
        [Range(0, int.MaxValue)]
        public int FanPitTicketsNumber { get; set; }
        [Required]
        [Range(0, int.MaxValue)]
        public int VipTicketsNumber { get; set; }
        [Required]
        [MinLength(1)]
        public string EventStreet { get; set; }
        [Required]
        public int EventStreetNumber { get; set; }
        [Required]
        [MinLength(1)]
        public string EventCity { get; set; }
        [Required]
        [Range(0, int.MaxValue)]
        public int EventCityZip { get; set; }
        [Required]
        public double EventGeographicWidth { get; set; }
        [Required]
        public double EventGeographicLength { get; set; }
        [Required]
        [MinLength(1)]
        public string FileName { get; set; }
        [Required]
        [Range(0, 31)]
        public int EventDay { get; set; }
        [Required]
        [Range(0, 12)]
        public int EventMonth { get; set; }
        [Required]
        [Range(2000, int.MaxValue)]
        public int EventYear { get; set; }
        [Required]
        [Range(0, 23)]
        public int EventHour { get; set; }
        [Required]
        [Range(0, 59)]
        public int EventMinutes { get; set; }
        [Required]
        [Range(0, 59)]
        public int EventSeconds { get; set; }
        [Required]
        [Range(0, double.MaxValue)]
        public double TicketPrice { get; set; }
    }
}