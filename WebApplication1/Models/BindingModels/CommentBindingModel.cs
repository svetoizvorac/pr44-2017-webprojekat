﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.BindingModels
{
    public class CommentBindingModel
    {
        [Required]
        public int CurrentUser { get; set; }
        [Required]
        [MinLength(1)]
        public string Text { get; set; }
        [Required]
        [Range(1,5)]
        public int Grade { get; set; }
        [Required]
        public bool IsApproved { get; set; }
        [Required]
        public bool IsDenied { get; set; }
        [Required]
        public int CurrentEvent { get; set; }
    }
}