﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.BindingModels
{
    public class UserSearchModel
    {
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Username { get; set; }
    }
}