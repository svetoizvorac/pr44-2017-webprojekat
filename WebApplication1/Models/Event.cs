///////////////////////////////////////////////////////////
//  Event.cs
//  Implementation of the Class Event
//  Generated by Enterprise Architect
//  Created on:      31-May-2020 8:05:50 PM
//  Original author: Predrag
///////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using WebApplication1.Models;

public class Event : SystemEntity {

    private Seller eventOrganizer;
	private Address eventAddress;
	private string eventCover;
	private DateTime eventDate;
	private EventType eventType;
	private bool isActive;
	private string name;
	private int numberOfParticipants;
    private int regularTickets;
    private int fanPitTickets;
    private int vipTickets;
    private double ticketBasePrice;
    private double averageGrade = -1;


    public Event(bool isActive, string eventCover, DateTime eventDate, EventType eventType, string name,
        int numberOfParticipants, double ticketBasePrice, Address eventAddress, Seller organizer, int regularTickets,
        int fanPitTickets, int vipTickets, double averageGrade, int iD, bool isDeleted) :base(iD, isDeleted)
    {
        this.IsActive = isActive;
        EventCover = eventCover;
        EventDate = eventDate;
        EventType = eventType;
        Name = name;
        NumberOfParticipants = numberOfParticipants;
        TicketBasePrice = ticketBasePrice;
        EventAddress = eventAddress;
        EventOrganizer = organizer;
        RegularTickets = regularTickets;
        FanPitTickets = fanPitTickets;
        VipTickets = vipTickets;
        AverageGrade = averageGrade;
    }


    public Event(Address eventAddress, string eventCover, DateTime eventDate, EventType eventType, 
        string name, int numberOfParticipants, double ticketBasePrice, Seller organizer,int regularTickets,
        int fanPitTickets, int vipTickets) : base()
    {
        this.eventAddress = eventAddress;
        this.EventCover = eventCover;
        this.EventDate = eventDate;
        this.EventType = eventType;
        this.Name = name;
        this.NumberOfParticipants = numberOfParticipants;
        this.TicketBasePrice = ticketBasePrice;
        EventOrganizer = organizer;
        RegularTickets = regularTickets;
        FanPitTickets = fanPitTickets;
        VipTickets = vipTickets;
    }

    public void Activate(){

        IsActive = true;
	}

	public void Deactivate(){

        IsActive = false;
	}



    public Address EventAddress { get => eventAddress; set => eventAddress = value; }
    public string EventCover { get => eventCover; set => eventCover = value; }
    public DateTime EventDate { get => eventDate; set => eventDate = value; }
    public EventType EventType { get => eventType; set => eventType = value; }
    public bool IsActive { get => isActive; set => isActive = value; }
    public string Name { get => name; set => name = value; }
    public int NumberOfParticipants { get => numberOfParticipants; set => numberOfParticipants = value; }
    public double TicketBasePrice { get => ticketBasePrice; set => ticketBasePrice = value; }
    public double AverageGrade { get => averageGrade; set => averageGrade = value; }
    public Seller EventOrganizer { get => eventOrganizer; set => eventOrganizer = value; }
    public int RegularTickets { get => regularTickets; set => regularTickets = value; }
    public int FanPitTickets { get => fanPitTickets; set => fanPitTickets = value; }
    public int VipTickets { get => vipTickets; set => vipTickets = value; }

    public override string Serialize()
    {
        return base.Serialize() +  string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}|{12}",
            EventAddress.ID, EventCover, EventDate.ToString("dd-MM-yyyy hh:mm:ss"), EventType.ToString(), IsActive, Name, NumberOfParticipants, TicketBasePrice, EventOrganizer.ID,
            RegularTickets, FanPitTickets, VipTickets, AverageGrade);
    }


}//end Event