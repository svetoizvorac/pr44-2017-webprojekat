///////////////////////////////////////////////////////////
//  Ticket.cs
//  Implementation of the Class Ticket
//  Generated by Enterprise Architect
//  Created on:      31-May-2020 8:05:51 PM
//  Original author: Predrag
///////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using WebApplication1.Models;

public class Ticket : SystemEntity {

	private string buyerName;
    private int buyerID;
	private Event _event;
	private bool isReserved;
	private DateTime cancelledDate;
	private string ticketID;
	private TicketType type;
    private double paidPrice;

    public Ticket(string buyerName, Event @event,string ticketID, TicketType type, bool isReserved, int buyerID, double paidPrice) : base()
    {
        BuyerName = buyerName;
        Event = @event;
        TicketID = ticketID;
        Type = type;
        BuyerID = buyerID;
        this.IsReserved = isReserved;
        PaidPrice = paidPrice;
    }

    public Ticket(string buyerName, Event @event, string ticketID, TicketType type, bool isReserved, int buyerID, int ID, bool isDeleted, double paidPrice, DateTime cancelledDate) : base(ID, isDeleted)
    {
        BuyerName = buyerName;
        Event = @event;
        TicketID = ticketID;
        Type = type;
        BuyerID = buyerID;
        this.IsReserved = isReserved;
        PaidPrice = paidPrice;
        CancelledDate = cancelledDate;
    }


    public string BuyerName{
		get{
			return buyerName;
		}
		set{
			buyerName = value;
		}
	}

    public void Cancell(){

        IsReserved = false;
        CancelledDate = DateTime.Now;

	}

    public void Reserve()
    {

        IsReserved = true;

    }

    public Event Event{
		get{
			return _event;
		}
		set{
			_event = value;
		}
	}


	public string TicketID{
		get{
			return ticketID;
		}
		set{
			ticketID = value;
		}
	}

	public TicketType Type{
		get{
			return type;
		}
		set{
			type = value;
		}
	}

    public int BuyerID { get => buyerID; set => buyerID = value; }
    public double PaidPrice { get => paidPrice; set => paidPrice = value; }
    public bool IsReserved { get => isReserved; set => isReserved = value; }
    public DateTime CancelledDate { get => cancelledDate; set => cancelledDate = value; }

    public override string Serialize()
    {
        return base.Serialize() + string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}", buyerName, BuyerID, _event.ID,  IsReserved, ticketID, type.ToString(), PaidPrice, CancelledDate);
    }

}//end Ticket