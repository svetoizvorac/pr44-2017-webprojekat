﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Repositories;

namespace WebApplication1.Models
{
    public abstract class SystemEntity
    {
        public int ID;
        public bool isDeleted;

        protected SystemEntity(int iD, bool isDeleted)
        {
            ID = iD;
            this.isDeleted = isDeleted;
        }

        protected SystemEntity()
        {
            ID = IDRepository.GenerateUniqueID();
            isDeleted = false;
        }

        public void Delete()
        {
            isDeleted = true;
        }

        public bool IsDeleted()
        {
            return isDeleted;
        }

        public virtual string Serialize()
        {
            return string.Format("{0}|{1}|", isDeleted, ID);
        }
    }
}