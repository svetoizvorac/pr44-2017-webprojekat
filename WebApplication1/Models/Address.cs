///////////////////////////////////////////////////////////
//  Address.cs
//  Implementation of the Class Address
//  Generated by Enterprise Architect
//  Created on:      31-May-2020 8:05:49 PM
//  Original author: Predrag
///////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using WebApplication1.Models;

public class Address : SystemEntity {

	private string city;
	private int number;
	private string street;
	private int zip;

	public Address(){

	}

    public Address(string city, int number, string street, int zip) : base()
    {
        City = city;
        Number = number;
        Street = street;
        Zip = zip;
    }


    public Address(string city, int number, string street, int zip, bool isDeleted, int ID) : base(ID, isDeleted)
    {
        City = city;
        Number = number;
        Street = street;
        Zip = zip;
    }

    ~Address(){

	}

	public string City{
		get{
			return city;
		}
		set{
			city = value;
		}
	}

	public int Number{
		get{
			return number;
		}
		set{
			number = value;
		}
	}

	public string Street{
		get{
			return street;
		}
		set{
			street = value;
		}
	}

	public int Zip{
		get{
			return zip;
		}
		set{
			zip = value;
		}
	}

    public override string Serialize()
    {
        return base.Serialize() +  string.Format("{0}|{1}|{2}|{3}", City, Number, Street, Zip);
    }
}//end Address