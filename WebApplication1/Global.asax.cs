﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.SessionState;
using WebApplication1.Repositories;

namespace WebApplication1
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            UserTypeRepository.InitRepo();
            UsersRepository.InitRepo();
            AddressRepository.InitRepo();
            LocationRepository.InitRepo();
            EventsRepository.InitRepo();
            CommentsRepository.InitRepo();
            TicketsRepository.InitRepo();
            
        }

        /// <summary>
        /// Dodaćemo još jedno procesiranje svakog zahteva: da dodamo podršku za Session u okviru REST poziva
        /// To radi funkcija <see cref="MyPostAuthenticateRequest(object, EventArgs)"/>
        /// </summary>
        public override void Init()
        {
            this.PostAuthenticateRequest += MyPostAuthenticateRequest;
            base.Init();
        }

        /// <summary>
        /// Uključuje podršku za Session, samo ako URL od zahteva počinje stringom "/api/"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void MyPostAuthenticateRequest(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.Url.AbsolutePath.StartsWith("/api/"))
            {
                System.Web.HttpContext.Current.SetSessionStateBehavior(
                SessionStateBehavior.Required);
            }
        }
    }
}
