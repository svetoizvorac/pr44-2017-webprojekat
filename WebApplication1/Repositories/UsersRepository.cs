﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace WebApplication1.Repositories
{
    public class UsersRepository
    {
        private static string adminsFile = HostingEnvironment.MapPath("~/App_Data/admins.txt");
        private static string buyersFile = HostingEnvironment.MapPath("~/App_Data/buyers.txt");
        private static string sellersFile = HostingEnvironment.MapPath("~/App_Data/sellers.txt");
        public static List<User> Users { get; set; }


        public static void InitRepo()
        {
            Users = new List<User>();
            DeserializeAll();
        }

        public bool EditUser(int ID, User newData)
        {
            User user = GetUserByID(ID);
            User potentialDuplicate = GetUser(newData.Username);

            if (user == null) return false;
            if (potentialDuplicate != null && potentialDuplicate.ID != user.ID) return false;

            RemoveUser(ID);
            user.Username = newData.Username;
            user.Name = newData.Name;
            user.LastName = newData.LastName;
            user.Password = newData.Password;
            user.Gender = newData.Gender;
            user.BirthDate = newData.BirthDate;
            AddUser(user);
            SerializeAllUsers();
            return true;
        }

        public bool DoesUserExist(string username)
        {
            return Users.Exists(x => x.Username == username && !x.IsDeleted());
        }

        public User GetUserByID(int ID)
        {
            return Users.Find(x => x.ID == ID && !x.IsDeleted());
        }

        public void UpdateBuyerPoints(int ID, int newPoints)
        {
            Buyer buyer = (Buyer)GetUserByID(ID);
            UserTypeRepository userTypeRepository = new UserTypeRepository();

            if (buyer == null) return;
            buyer.NumberOfPoints += newPoints;
            if (buyer.NumberOfPoints < 0) buyer.NumberOfPoints = 0;

            if(buyer.NumberOfPoints >= 4000)
            {
                buyer.Type = userTypeRepository.GetTypeByName("Zlatni");
            }else if(buyer.NumberOfPoints >= 2000)
            {
                buyer.Type = userTypeRepository.GetTypeByName("Srebrni");
            }
            else
            {
                buyer.Type = userTypeRepository.GetTypeByName("Bronzani");
            }
            RemoveUser(buyer.ID);
            AddUser(buyer);
            SerializeAllUsers();
        }

        public void AddUser(User user)
        {
            Users.Add(user);
        }


        public bool CreateUser(User user)
        {
            if (!DoesUserExist(user.Username))
            {
                Users.Add(user);
                SerializeUser(user, GetFilenameForUser(user));
                return true;
            }

            return false;

        }

        public bool LoginUser(string username, string password)
        {
            if (!DoesUserExist(username)) return false;
            User user = GetUser(username);

            if (user.LogIn(password))
            {
                RemoveUser(user.ID);
                Users.Add(user);
                return true;
            }
            else return false;
            
        }

        internal bool DeleteUserByID(int ID)
        {
            User user = GetUserByID(ID);

            if (user == null) return false;

            RemoveUser(user.ID);
            user.Delete();
            AddUser(user);
            SerializeAllUsers();

            //Delete related data here
            if(user.isBuyer())
            {
                TicketsRepository repo = new TicketsRepository();
                repo.DeleteBuyersTickets(user.ID);
            }

            if(user.isSeller())
            {
                EventsRepository repo = new EventsRepository();
                repo.DeleteSellersEvents(user.ID);
            }
            return true;
        }

        public void EvaluateSuspiciousness(int userID)
        {
            try
            {
                Buyer buyer = (Buyer)GetUserByID(userID);
                if (buyer.IsSuspicious) return;

                TicketsRepository repository = new TicketsRepository();
                List<Ticket> usersTickets = repository.GetValidTicketsByBuyerID(userID);

                int cancelledTicketCount = 0;
                DateTime currentDate = DateTime.Now;

                foreach (Ticket ticket in usersTickets)
                {
                    if (!ticket.IsReserved && (currentDate - ticket.CancelledDate).Days <= 30) cancelledTicketCount++;

                    if(cancelledTicketCount >= 5)
                    {
                        RemoveUser(userID);
                        buyer.IsSuspicious = true;
                        Users.Add(buyer);
                        SerializeAllUsers();
                        return;
                    }
                }
            }
            catch
            {
                return;
            }
            
        }

        public bool LogoutUser(string username)
        {
            if (!DoesUserExist(username)) return false;
            User user = GetUser(username);

            if (user.LogOut())
            {
                RemoveUser(user.ID);
                Users.Add(user);
                return true;
            }
            else return false;

        }

        public bool BlockUserByID(int ID)
        {
            User user = GetUserByID(ID);

            if (user == null || user.isBlocked()) return false;

            RemoveUser(user.ID);
            user.Block();
            AddUser(user);
            SerializeAllUsers();
            return true;
        }

        public List<User> GetAllUsers()
        {
            return Users.FindAll(x => !x.IsDeleted()).ToList();
        }


        public User GetUser(string username)
        {
            return Users.Find(x => x.Username == username);
        }


        public void RemoveUser(int ID)
        {
            Users.RemoveAll(x => x.ID == ID);
        }

        private void SerializeAllUsers()
        {
            File.Delete(adminsFile);
            File.Delete(buyersFile);
            File.Delete(sellersFile);

            foreach (User user in Users)
            {
                string filename = GetFilenameForUser(user);
                SerializeUser(user, filename);

            }
        }


        private void SerializeUser(User user, string fileName)
        {
            using (StreamWriter sr = File.AppendText(fileName))
            {
                sr.WriteLine(user.Serialize());
            }
        }

        private static void DeserializeAll()
        {

            /*Deserialize admins*/
            UserTypeRepository typeRepo = new UserTypeRepository();

            List<string> lines = File.ReadLines(adminsFile).ToList();

            foreach(string line in lines)
            {
                if (string.IsNullOrEmpty(line)) continue;

                string[] parameters = line.Split('|');

                if (parameters.Length < 9) continue;
                try
                {
                    bool isDeleted = bool.Parse(parameters[0]);
                    int ID = int.Parse(parameters[1]);
                    string userName = parameters[2];
                    string name = parameters[3];
                    string lastName = parameters[4];
                    Gender gender = (Gender)Enum.Parse(typeof(Gender), parameters[5]);
                    string password = parameters[6];
                    DateTime birthDate = DateTime.Parse(parameters[7]);
                    bool isblocked = bool.Parse(parameters[8]);

                    Administrator admin = new Administrator(birthDate, gender, lastName, name, password, userName, ID, isDeleted);
                    if (isblocked) admin.Block();
                    Users.Add(admin);

                }catch(Exception e)
                {

                }
            }

            /*Deserialize buyers*/
            lines = File.ReadLines(buyersFile).ToList();

            foreach (string line in lines)
            {
                if (string.IsNullOrEmpty(line)) continue;

                string[] parameters = line.Split('|');

                if (parameters.Length < 12) continue;
                try
                {
                    bool isDeleted = bool.Parse(parameters[0]);
                    int ID = int.Parse(parameters[1]);
                    string userName = parameters[2];
                    string name = parameters[3];
                    string lastName = parameters[4];
                    Gender gender = (Gender)Enum.Parse(typeof(Gender), parameters[5]);
                    string password = parameters[6];
                    DateTime birthDate = DateTime.Parse(parameters[7]);
                    bool isblocked = bool.Parse(parameters[8]);
                    int points = int.Parse(parameters[9]);
                    string type = parameters[10];
                    bool isSuspicious = bool.Parse(parameters[11]);

                    UserType userType = typeRepo.GetTypeByName(type);
                    Buyer buyer = new Buyer(birthDate, gender, lastName, name, password, userName, ID, isDeleted, points, userType, isSuspicious);
                    if (isblocked) buyer.Block();
                    Users.Add(buyer);

                }
                catch (Exception e)
                {

                }
            }


            /*Deserialize sellers*/
            lines = File.ReadLines(sellersFile).ToList();

            foreach (string line in lines)
            {
                if (string.IsNullOrEmpty(line)) continue;

                string[] parameters = line.Split('|');

                if (parameters.Length < 9) continue;
                try
                {
                    bool isDeleted = bool.Parse(parameters[0]);
                    int ID = int.Parse(parameters[1]);
                    string userName = parameters[2];
                    string name = parameters[3];
                    string lastName = parameters[4];
                    Gender gender = (Gender)Enum.Parse(typeof(Gender), parameters[5]);
                    string password = parameters[6];
                    DateTime birthDate = DateTime.Parse(parameters[7]);
                    bool isblocked = bool.Parse(parameters[8]);

                    Seller seller = new Seller(birthDate, gender, lastName, name, password, userName, ID, isDeleted);
                    if (isblocked) seller.Block();
                    Users.Add(seller);

                }
                catch (Exception e)
                {

                }
            }

        }
    
        public static string GetFilenameForUser(User user)
        {
            if (user.isAdmin())
            {
                return adminsFile;
            }
            else if (user.isBuyer())
            {
                return buyersFile;
            }
            else if (user.isSeller())
            {
                return sellersFile;
            }

            return "";
        }
    }

}