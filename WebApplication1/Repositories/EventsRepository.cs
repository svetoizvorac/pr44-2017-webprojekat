﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace WebApplication1.Repositories
{
    public class EventsRepository
    {

        private static string eventsFile = HostingEnvironment.MapPath("~/App_Data/events.txt");
        public static List<Event> Events { get; set; }


        public static void InitRepo()
        {
            Events = new List<Event>();
            DeserializeAll();
        }

        public bool CreateEvent(Event _event)
        {
            if (GetEventByID(_event.ID) != null) return false;
            AddEvent(_event);
            SerializeEvent(_event, eventsFile);
            return true;
        }

        public void CalculateEventAverageGrade(int ID)
        {
            Event _event = GetEventByID(ID);
            if (_event == null) return;
            CommentsRepository commentsRepository = new CommentsRepository();
            List<Comment> approvedComments = commentsRepository.GetApprovedCommentsByEventID(ID);

            double totalPoints = 0;
            foreach(Comment comment in approvedComments)
            {
                totalPoints += comment.Grade;
            }

            RemoveEventByID(ID);
            _event.AverageGrade = Math.Round((totalPoints / approvedComments.Count), 1);
            AddEvent(_event);
            SerializeAllEvents();
        }

        public void AddEvent(Event _event)
        {
            Events.Add(_event);
        }

        public Event GetEventByID(int ID)
        {
            return Events.Find(x => x.ID == ID && !x.IsDeleted());
        }

        public List<Event> GetAllEvents()
        {
            return Events.FindAll(x => !x.IsDeleted());
        }

        public List<Event> GetAllActiveEvents()
        {
            return Events.FindAll(x => !x.IsDeleted() && x.IsActive);
        }

        public List<Event> GetEventsBySellerID(int ID)
        {
            return Events.FindAll(x => !x.IsDeleted() && x.EventOrganizer.ID == ID);
        }

        public void RemoveEventByID(int ID)
        {
            Events.RemoveAll(x => x.ID == ID);
        }

        public bool DeleteEventByID(int ID)
        {
            Event @event = Events.Find(x => x.ID == ID && !x.IsDeleted());

            if (@event == null) return false;
            @event.Delete();
            RemoveEventByID(ID);
            AddEvent(@event);
            SerializeAllEvents();
            TicketsRepository ticketsRepository = new TicketsRepository();
            ticketsRepository.DeleteEventTickets(@event.ID);

            return true;
        }

        private void SerializeAllEvents()
        {
            File.Delete(eventsFile);

            foreach (Event _event in Events)
            {
                SerializeEvent(_event, eventsFile);

            }
        }

        public bool ApproveEventByID(int ID)
        {
            Event @event = GetEventByID(ID);

            if (@event == null || @event.IsActive) return false;

            @event.Activate();
            RemoveEventByID(ID);
            AddEvent(@event);
            SerializeAllEvents();
            return true;
        }

        public bool DoesEventExistOnCoordinatesAndTime(DateTime eventTime, Address address)
        {
            List<Event> events = GetAllEvents();
            LocationRepository repo = new LocationRepository();

            foreach(Event @event in events)
            {
                Location location = repo.GetLocationByAddressID(@event.EventAddress.ID);
                if((@event.EventDate - eventTime).Days == 0  && location.Address.Street == address.Street && location.Address.Number == address.Number
                    && location.Address.City == address.City)
                {
                    return true;
                }
            }

            return false;
        }

        private void SerializeEvent(Event _event, string fileName)
        {
            using (StreamWriter sr = File.AppendText(fileName))
            {
                sr.WriteLine(_event.Serialize());
            }
        }

        private static void DeserializeAll()
        {
            AddressRepository repo = new AddressRepository();
            UsersRepository usersRepo = new UsersRepository();
            List<string> lines = File.ReadLines(eventsFile).ToList();

            foreach (string line in lines)
            {
                if (string.IsNullOrEmpty(line)) continue;

                string[] parameters = line.Split('|');

                if (parameters.Length < 15) continue;
                try
                {
                    bool isDeleted = bool.Parse(parameters[0]);
                    int ID = int.Parse(parameters[1]);
                    int addressID = int.Parse(parameters[2]);
                    string eventCover = parameters[3];
                    DateTime eventDate = DateTime.Parse(parameters[4]);
                    EventType type = (EventType)Enum.Parse(typeof(EventType), parameters[5]);
                    bool isActive = bool.Parse(parameters[6]);
                    string name = parameters[7];
                    int numberOfParticipants = int.Parse(parameters[8]);
                    double ticketbasePrice = double.Parse(parameters[9]);
                    int sellerID = int.Parse(parameters[10]);
                    int regular = int.Parse(parameters[11]);
                    int fanPit = int.Parse(parameters[12]);
                    int vip = int.Parse(parameters[13]);
                    double averageGrade = double.Parse(parameters[14]);
                    Address address = repo.GetAddressByID(addressID);
                    Seller seller = (Seller)usersRepo.GetUserByID(sellerID);

                    Event newEvent = new Event(isActive, eventCover, eventDate, type, name, numberOfParticipants, ticketbasePrice, address, seller,
                                                regular, fanPit, vip, averageGrade, ID, isDeleted);
                    Events.Add(newEvent);


                }
                catch (Exception e)
                {

                }
            }

        }

        public void DeleteSellersEvents(int ID)
        {
            List<Event> events = GetEventsBySellerID(ID);
            foreach(Event ev in events)
            {
                DeleteEventByID(ev.ID);
            }
        }

        public void ChangeEventTicketsNumber(int ID, int regularTickets, int fanPitTickets, int vipTickets)
        {
            Event @event = GetEventByID(ID);

            @event.RegularTickets += regularTickets;
            @event.FanPitTickets += fanPitTickets;
            @event.VipTickets += vipTickets;
            RemoveEventByID(ID);
            AddEvent(@event);
            SerializeAllEvents();
        }

        public void UpdateEvent(Event newEvent)
        {
            Event oldEvent = GetEventByID(newEvent.ID);

            if (oldEvent == null) return;
            RemoveEventByID(oldEvent.ID);
            AddEvent(newEvent);
            SerializeAllEvents();
        }
    }
}