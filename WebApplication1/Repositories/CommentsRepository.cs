﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace WebApplication1.Repositories
{
    public class CommentsRepository
    {
        private static string commentsFile = HostingEnvironment.MapPath("~/App_Data/comments.txt");
        public static List<Comment> Comments { get; set; }


        public static void InitRepo()
        {
            Comments = new List<Comment>();
            DeserializeAll();
        }


        public void AddComment(Comment comment)
        {
            Comments.Add(comment);
        }

        public bool CreateComment(Comment comment)
        {
            if (Comments.Exists(x => x.ID == comment.ID || (x.User == comment.User && x.Event.ID == comment.Event.ID) && !x.IsDeleted())) return false;

            AddComment(comment);
            SerializeComment(comment, commentsFile);
            return true;
        }

        public Comment GetCommentByID(int ID)
        {
            return Comments.Find(x => x.ID == ID && !x.IsDeleted());
        }

        public List<Comment> GetValidCommentsByEventID(int ID)
        {
            return Comments.FindAll(x => x.Event.ID == ID && !x.IsDeleted());
        }

        public List<Comment> GetApprovedCommentsByEventID(int ID)
        {
            return Comments.FindAll(x => x.Event.ID == ID && !x.IsDeleted() && x.IsApproved);
        }

        public bool ApproveComment(int ID)
        {
            Comment comment = GetCommentByID(ID);
            if (comment == null || comment.IsDenied) return false;

            comment.Approve();
            RemoveCommentByID(ID);
            AddComment(comment);
            SerializeAllComments();
            return true;
        }

        public bool DenyComment(int ID)
        {
            Comment comment = GetCommentByID(ID);
            if (comment == null || comment.IsApproved) return false;

            comment.Deny();
            RemoveCommentByID(ID);
            AddComment(comment);
            SerializeAllComments();
            return true;
        }

        public bool DeleteComment(int ID)
        {
            Comment comment = GetCommentByID(ID);
            if (comment == null) return false;

            comment.Delete();
            RemoveCommentByID(ID);
            AddComment(comment);
            SerializeAllComments();
            return true;
        }


        public void RemoveCommentByID(int ID)
        {
            Comments.RemoveAll(x => x.ID == ID);
        }

        private void SerializeAllComments()
        {
            File.Delete(commentsFile);

            foreach (Comment comment in Comments)
            {
                SerializeComment(comment, commentsFile);

            }
        }


        private void SerializeComment(Comment comment, string fileName)
        {
            using (StreamWriter sr = File.AppendText(fileName))
            {
                sr.WriteLine(comment.Serialize());
            }
        }

        private static void DeserializeAll()
        {
            EventsRepository eventsRepository = new EventsRepository();
            UsersRepository usersRepository = new UsersRepository();

            List<string> lines = File.ReadLines(commentsFile).ToList();

            foreach (string line in lines)
            {
                if (string.IsNullOrEmpty(line)) continue;

                string[] parameters = line.Split('|');

                if (parameters.Length < 8) continue;
                try
                {
                    bool isDeleted = bool.Parse(parameters[0]);
                    int ID = int.Parse(parameters[1]);
                    int eventID = int.Parse(parameters[2]);
                    int userID = int.Parse(parameters[3]);
                    bool isApproved = bool.Parse(parameters[4]);
                    string text = parameters[5];
                    int grade = int.Parse(parameters[6]);
                    bool isDenied= bool.Parse(parameters[7]);
                    Event _event = eventsRepository.GetEventByID(eventID);
                    User user = usersRepository.GetUserByID(userID);

                    Comment comment = new Comment(isApproved, grade, text, user, _event, isDenied, ID, isDeleted);
                    Comments.Add(comment);

                }
                catch (Exception e)
                {

                }
            }


        }
    }
}