﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace WebApplication1.Repositories
{
    public class LocationRepository
    {
        private static string locationsFile = HostingEnvironment.MapPath("~/App_Data/locations.txt");
        public static List<Location> Locations { get; set; }


        public static void InitRepo()
        {
            Locations = new List<Location>();
            DeserializeAll();
        }


        //Adds location if it doesn't exist, if it exists returns existing location ID
        public void CreateLocation(Location location)
        {

            AddLocation(location);
            SerializeLocation(location, locationsFile);
        }

        public bool DoesLocationExist(Location location)
        {
            return Locations.Exists(x => x.GeographicLength == location.GeographicLength && x.GeographicWidth == x.GeographicWidth  && !x.isDeleted);
        }


        public int GetExistingLocationID(Location location)
        {
            return Locations.Find(x => x.GeographicLength == location.GeographicLength && x.GeographicWidth == x.GeographicWidth && !x.isDeleted).ID;
        }

        public void AddLocation(Location location)
        {
            Locations.Add(location);
        }

        public Location GetLocationByID(int ID)
        {
            return Locations.Find(x => x.ID == ID && !x.IsDeleted());
        }

        public Location GetLocationByAddressID(int ID)
        {
            return Locations.Find(x => x.Address.ID == ID && !x.IsDeleted());
        }


        public void RemoveLocationByID(int ID)
        {
            Locations.RemoveAll(x => x.ID == ID);
        }

        private void SerializeAllLocations()
        {
            File.Delete(locationsFile);

            foreach (Location location in Locations)
            {
                SerializeLocation(location, locationsFile);

            }
        }


        private void SerializeLocation(Location location, string fileName)
        {
            using (StreamWriter sr = File.AppendText(fileName))
            {
                sr.WriteLine(location.Serialize());
            }
        }

        private static void DeserializeAll()
        {
            AddressRepository repo = new AddressRepository();
            List<string> lines = File.ReadLines(locationsFile).ToList();

            foreach (string line in lines)
            {
                if (string.IsNullOrEmpty(line)) continue;

                string[] parameters = line.Split('|');

                if (parameters.Length < 5) continue;
                try
                {
                    bool isDeleted = bool.Parse(parameters[0]);
                    int ID = int.Parse(parameters[1]);
                    int addressID = int.Parse(parameters[2]);
                    double geographicLength = double.Parse(parameters[3]);
                    double geographicWidth= double.Parse(parameters[4]);
                    Address address = repo.GetAddressByID(addressID);
                    if (address == null) continue;
                    Location location = new Location(geographicLength, geographicWidth, address, ID, isDeleted);
                    Locations.Add(location);

                }
                catch (Exception e)
                {

                }
            }


        }
    }
}