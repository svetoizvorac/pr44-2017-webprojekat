﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace WebApplication1.Repositories
{
    public class AddressRepository
    {

        private static string addressFile = HostingEnvironment.MapPath("~/App_Data/address.txt");
        public static List<Address> Addresses { get; set; }


        public static void InitRepo()
        {
            Addresses = new List<Address>();
            DeserializeAll();
        }

        //Adds address if it doesn't exist, if it exists returns existing address ID
        public int CreateAddress(Address address)
        {
            if(!DoesAddressExist(address))
            {
                AddAddress(address);
                SerializeAddress(address, addressFile);
                return address.ID;
            }

            return GetExistingAddressID(address);
        }

        public bool DoesAddressExist(Address address)
        {
            return Addresses.Exists(x => x.Street == address.Street && x.Number == address.Number && x.Zip == address.Zip && x.City == address.City && !x.isDeleted);
        }


        public int GetExistingAddressID(Address address)
        {
            return Addresses.Find(x => x.Street == address.Street && x.Number == address.Number && x.Zip == address.Zip && x.City == address.City && !x.isDeleted).ID;
        }

        public void AddAddress(Address address)
        {
            Addresses.Add(address);
        }

        public Address GetAddressByID(int ID)
        {
            return Addresses.Find(x => x.ID == ID && !x.IsDeleted());
        }


        public void RemoveAddressByID(int ID)
        {
            Addresses.RemoveAll(x => x.ID == ID);
        }

        private void SerializeAllAddresses()
        {
            File.Delete(addressFile);

            foreach (Address address in Addresses)
            {
                SerializeAddress(address, addressFile);

            }
        }


        private void SerializeAddress(Address address, string fileName)
        {
            using (StreamWriter sr = File.AppendText(fileName))
            {
                sr.WriteLine(address.Serialize());
            }
        }

        private static void DeserializeAll()
        {

            List<string> lines = File.ReadLines(addressFile).ToList();

            foreach (string line in lines)
            {
                if (string.IsNullOrEmpty(line)) continue;

                string[] parameters = line.Split('|');

                if (parameters.Length < 6) continue;
                try
                {
                    bool isDeleted = bool.Parse(parameters[0]);
                    int ID = int.Parse(parameters[1]);
                    string city = parameters[2];
                    int number = int.Parse(parameters[3]);
                    string street = parameters[4];
                    int zip = int.Parse(parameters[5]);

                    Address address = new Address(city, number, street, zip, isDeleted, ID);
                    Addresses.Add(address);

                }
                catch (Exception e)
                {

                }
            }


        }

    }
}