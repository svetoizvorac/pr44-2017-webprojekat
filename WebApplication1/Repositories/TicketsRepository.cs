﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace WebApplication1.Repositories
{
    public class TicketsRepository
    {
        private static string ticketsFile = HostingEnvironment.MapPath("~/App_Data/tickets.txt");
        public static List<Ticket> Tickets { get; set; }


        public static void InitRepo()
        {
            Tickets = new List<Ticket>();
            DeserializeAll();
        }


        public void AddTicket(Ticket ticket)
        {
            Tickets.Add(ticket);
        }


        public void UpdateTicketsBuyerName(int buyerID, string newName)
        {

            List<Ticket> userTickets = GetValidTicketsByBuyerID(buyerID);

            foreach(Ticket ticket in userTickets)
            {
                RemoveTicketByID(ticket.ID);
                ticket.BuyerName = newName;
                AddTicket(ticket);
            }
            SerializeAllTickets();
        }


        public void UpdateTicketsEventData(int eventID, Event newData)
        {
            List<Ticket> userTickets =  GetValidTicketsByEventID(eventID);

            foreach (Ticket ticket in userTickets)
            {
                RemoveTicketByID(ticket.ID);
                ticket.Event = newData;
                AddTicket(ticket);
            }
            SerializeAllTickets();
        }


        public bool CancellTicket(int ID)
        {
            EventsRepository eventsRepository = new EventsRepository();
            Ticket ticket = GetTicketByID(ID);
            if (ticket == null || ticket.IsReserved == false) return false;

            DateTime currentDate = DateTime.Now;
            if ((ticket.Event.EventDate - currentDate).TotalDays < 7) return false;

            switch (ticket.Type)
            {
                case TicketType.REGULAR:
                    eventsRepository.ChangeEventTicketsNumber(ticket.Event.ID, 1, 0, 0);
                    break;
                case TicketType.FAN_PIT:
                    eventsRepository.ChangeEventTicketsNumber(ticket.Event.ID, 0, 1, 0);
                    break;
                case TicketType.VIP:
                    eventsRepository.ChangeEventTicketsNumber(ticket.Event.ID, 0, 0, 1);
                    break;
            }
            ticket.Cancell();
            RemoveTicketByID(ID);
            AddTicket(ticket);
            SerializeAllTickets();
            return true;
        }

        public bool DoesUserHaveReservedTickets(int userID, int eventID)
        {
            return Tickets.Exists(x => x.BuyerID == userID && x.Event.ID == eventID && x.IsReserved);
        }

        public Ticket GetTicketByID(int ID)
        {
            return Tickets.Find(x => x.ID == ID && !x.IsDeleted());
        }

        public Ticket GetTicketByTicketID(string ID)
        {
            return Tickets.Find(x => x.TicketID == ID && !x.IsDeleted());
        }

        public List<Ticket> GetValidTicketsByEventID(int ID)
        {
            return Tickets.FindAll(x => x.Event.ID == ID && !x.IsDeleted());
        }

        public List<Ticket> GetValidTicketsByBuyerID(int ID)
        {
            return Tickets.FindAll(x => x.BuyerID == ID && !x.IsDeleted());
        }

        public void DeleteEventTickets(int ID)
        {
            List<Ticket> eventTickets = GetValidTicketsByEventID(ID);
            foreach(Ticket ticket in eventTickets)
            {
                RemoveTicketByID(ticket.ID);
                ticket.Delete();
                AddTicket(ticket);
            }

            SerializeAllTickets();
        }

        public bool DeleteTicketByID(int ID)
        {
            Ticket ticket = GetTicketByID(ID);

            if (ticket == null || ticket.IsDeleted()) return false;
            RemoveTicketByID(ticket.ID);
            ticket.Delete();
            AddTicket(ticket);
            SerializeAllTickets();
            return true;

        }

        public List<Ticket> GetAllTickets()
        {
            return Tickets.FindAll(x => !x.IsDeleted()).ToList();
        }

        public List<Ticket> GetValidTicketsBySellerID(int ID)
        {
            return Tickets.FindAll(x => x.Event.EventOrganizer.ID == ID && !x.IsDeleted());
        }

        public void DeleteBuyersTickets(int ID)
        {
            List<Ticket> tickets = GetValidTicketsByBuyerID(ID);

            foreach (Ticket ticket in tickets) DeleteTicketByID(ticket.ID);
        }

        public void RemoveTicketByID(int ID)
        {
            Tickets.RemoveAll(x => x.ID == ID);
        }

        public void SerializeAllTickets()
        {
            File.Delete(ticketsFile);

            foreach (Ticket ticket in Tickets)
            {
                SerializeTicket(ticket, ticketsFile);

            }
        }


        private void SerializeTicket(Ticket ticket, string fileName)
        {
            using (StreamWriter sr = File.AppendText(fileName))
            {
                sr.WriteLine(ticket.Serialize());
            }
        }

        private static void DeserializeAll()
        {
            EventsRepository eventsRepository = new EventsRepository();

            List<string> lines = File.ReadLines(ticketsFile).ToList();

            foreach (string line in lines)
            {
                if (string.IsNullOrEmpty(line)) continue;

                string[] parameters = line.Split('|');

                if (parameters.Length < 9) continue;
                try
                {
                    bool isDeleted = bool.Parse(parameters[0]);
                    int ID = int.Parse(parameters[1]);
                    string buyername = parameters[2];
                    int buyerID = int.Parse(parameters[3]);
                    int eventID = int.Parse(parameters[4]);
                    bool isReserved = bool.Parse(parameters[5]);
                    string ticketID = parameters[6];
                    TicketType type = (TicketType)Enum.Parse(typeof(TicketType), parameters[7]);
                    double paidPrice = double.Parse(parameters[8]);
                    DateTime cancelledDate;
                    if (parameters.Count() > 9)
                    {
                        cancelledDate = DateTime.Parse(parameters[9]);
                    }
                    else
                    {
                        cancelledDate = new DateTime();
                    }
                    

                    Event @event = eventsRepository.GetEventByID(eventID);

                    Ticket Ticket = new Ticket(buyername, @event,ticketID, type, isReserved, buyerID, ID, isDeleted, paidPrice, cancelledDate);
                    Tickets.Add(Ticket);

                }
                catch (Exception e)
                {

                }
            }


        }

        public bool CreateReservation(Ticket ticket, bool commit)
        {
            Ticket possibleDuplicate = GetTicketByTicketID(ticket.TicketID);

            if (possibleDuplicate != null) return false;

            AddTicket(ticket);
            if(commit)
                SerializeTicket(ticket, ticketsFile);
            return true;
        }
    }
}