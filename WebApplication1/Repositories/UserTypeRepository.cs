﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Repositories
{
    public class UserTypeRepository
    {

        /*This is a hardcoded list, there is no reason for this to be kept inside a file. User types are not configurable 
         * according to project requirements*/
        public static Dictionary<string, UserType> UserTypes { get; set; }

        public static void InitRepo()
        {
            UserTypes = new Dictionary<string, UserType>();
            UserTypes.Add("Zlatni", new UserType(5, 4000, "Zlatni"));
            UserTypes.Add("Srebrni", new UserType(3, 2000, "Srebrni"));
            UserTypes.Add("Bronzani", new UserType(0, 0, "Bronzani"));
        }

        public UserType GetTypeByName(string name)
        {
            if (!UserTypes.ContainsKey(name)) return null;
            return UserTypes[name];
        }
    }
}