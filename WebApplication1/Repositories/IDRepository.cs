﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Hosting;

namespace WebApplication1.Repositories
{
    public static class IDRepository
    {
        private static string lastIDfile = HostingEnvironment.MapPath("~/App_Data/id.txt");


        public static int GenerateUniqueID()
        {
            int lastID;
            using (StreamReader sr = File.OpenText(lastIDfile))
            {
                lastID = int.Parse(sr.ReadLine());
            }
            lastID++;
            File.Delete(lastIDfile);
            using (StreamWriter sr = File.AppendText(lastIDfile))
            {
                sr.WriteLine(lastID);
            }

            return lastID;
            
        }


        public static byte[] GetHash(string inputString)
        {
            using (HashAlgorithm algorithm = SHA256.Create())
                return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }
    }
}