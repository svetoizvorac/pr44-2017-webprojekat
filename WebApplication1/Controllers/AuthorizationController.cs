﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebApplication1.Models.BindingModels;
using WebApplication1.Repositories;

namespace WebApplication1.Controllers
{
    [RoutePrefix("api/Authorization")]
    public class AuthorizationController : ApiController
    {
        UsersRepository repository = new UsersRepository();
        UserTypeRepository typeRepo = new UserTypeRepository();
        TicketsRepository ticketsRepository = new TicketsRepository();
        IAccessRights accessRights = new AccessRightsResolver();


        [HttpPost]
        [Route("Register")]
        public HttpResponseMessage Register([FromBody] RegistrationBindingModel registration)
        {
            if(!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Submitted data is not valid, could not create user!");
            }
            DateTime birthDate = new DateTime(registration.Birthyear, registration.Birthmonth, registration.Birthday);
            User newUser = null;
            if(registration.IsSeller)
            {
                User user = (User)HttpContext.Current.Session["user"];
                if (!accessRights.CanView(user, Functionalities.SELLER_CREATION))
                {
                    return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
                }
                newUser = new Seller(birthDate, registration.Gender, registration.Lastname, registration.Name, registration.Password, registration.Username);
            }
            else
            {
                newUser = new Buyer(birthDate, registration.Gender, registration.Lastname, registration.Name, registration.Password, registration.Username, typeRepo.GetTypeByName("Bronzani"));
            }

            if(!repository.CreateUser(newUser))
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Could not register this user, maybe he/she already exists");
            }

            return Request.CreateResponse(HttpStatusCode.OK, "User created successfully");
        }


        [HttpPost]
        [Route("EditUser")]
        public HttpResponseMessage EditUser([FromBody] RegistrationBindingModel registration)
        {
            User principal = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(principal, Functionalities.VIEW_OWN_PROFILE))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }

            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Nevalidni podaci! Nije moguce izmeniti korisnikove podatke.");
            }
            DateTime birthDate = new DateTime(registration.Birthyear, registration.Birthmonth, registration.Birthday);
            

            User newUser = new Seller(birthDate, registration.Gender, registration.Lastname, registration.Name, registration.Password, registration.Username, principal.ID, principal.isDeleted);
        

            if (!repository.EditUser(principal.ID, newUser))
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Promena podataka neuspesna. Moguce je da je korisnik izbrisan ili je uneto korisnicko ime zauzeto.");
            }

            if(principal.isBuyer())
            {
                ticketsRepository.UpdateTicketsBuyerName(principal.ID, principal.Name + " " + principal.LastName);
            }

            HttpContext.Current.Session["user"] = repository.GetUserByID(principal.ID);

            return Request.CreateResponse(HttpStatusCode.OK, "Podaci izmenjeni uspesno");
        }

        // GET: api/Authorization/CanView
        [HttpGet]
        [Route("CanView")]
        public HttpResponseMessage CanView([FromUri]Functionalities functionality)
        {
            var user = HttpContext.Current.Session["user"];

            User principal = (User)user;
            if(accessRights.CanView(principal, functionality))
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden);
            }
        }


        [HttpGet]
        [Route("Logout")]
        public HttpResponseMessage Logout()
        {
            User principal = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(principal, Functionalities.LOG_OUT))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }

            repository.LogoutUser(principal.Username);
            HttpContext.Current.Session["user"] = null;
            var response =  Request.CreateResponse(HttpStatusCode.OK);
            return response;

        }


        [HttpGet]
        [Route("GetCurrentUser")]
        public User GetCurrentUser()
        {
            //No authorization required
            var user = HttpContext.Current.Session["user"];
            User principal = (User)user;

            return principal;
        }


        [HttpPost]
        [Route("Login")]
        public HttpResponseMessage Login([FromBody]LoginBindingModel bindingModel)
        {
            User principal = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(principal, Functionalities.LOG_IN))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }

            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Log in failed, invalid data.");
            }

            if(!repository.DoesUserExist(bindingModel.Username))
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Log in failed, Do you really have an account?.");
            }

            if(!repository.LoginUser(bindingModel.Username, bindingModel.Password))
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Log in failed, please check your credentials again.");
            }

            HttpContext.Current.Session["user"] = repository.GetUser(bindingModel.Username);
            return Request.CreateResponse(HttpStatusCode.OK);

        }
    }
}
