﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebApplication1.Models.BindingModels;
using WebApplication1.Repositories;

namespace WebApplication1.Controllers
{
    [RoutePrefix("api/Admin")]
    public class AdminController : ApiController
    {
        TicketsRepository ticketsRepo = new TicketsRepository();
        UsersRepository usersRepository = new UsersRepository();
        EventsRepository eventsRepository = new EventsRepository();
        CommentsRepository commentsRepository = new CommentsRepository();
        IAccessRights accessRights = new AccessRightsResolver();

        [HttpGet]
        [Route("GetAllEvents")]
        public HttpResponseMessage GetAllEvents()
        {
            User user = (User)HttpContext.Current.Session["user"];
            if(!accessRights.CanView(user, Functionalities.ACTIVATE_EVENT))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }
            return Request.CreateResponse(HttpStatusCode.OK, eventsRepository.GetAllEvents().OrderBy(x => x.EventDate).ToList());
        }


        [HttpGet]
        [Route("GetAllTickets")]
        public HttpResponseMessage GetAllTickets()
        {
            User user = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(user, Functionalities.VIEW_SYTEM_WIDE_TICKETS))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }
            return Request.CreateResponse(HttpStatusCode.OK, ticketsRepo.GetAllTickets());
        }

        [HttpGet]
        [Route("GetAllUsers")]
        public HttpResponseMessage GetAllUsers()
        {
            User user = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(user, Functionalities.VIEW_ALL_SYSTEM_USERS))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }
            return Request.CreateResponse(HttpStatusCode.OK, usersRepository.GetAllUsers());
        }

        [HttpGet]
        [Route("DeleteEvent")]
        public HttpResponseMessage DeleteEvent([FromUri] int ID)
        {
            User user = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(user, Functionalities.DELETE_DATA))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }
            if (eventsRepository.DeleteEventByID(ID))
            {
                return Request.CreateResponse(HttpStatusCode.OK, "Dogadjaj obrisan");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden);
            }
        }

        [HttpGet]
        [Route("DeleteUser")]
        public HttpResponseMessage DeleteUser([FromUri] int ID)
        {
            User user = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(user, Functionalities.DELETE_DATA))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }
            if (usersRepository.DeleteUserByID(ID))
            {
                return Request.CreateResponse(HttpStatusCode.OK, "Korisnik obrisan. Obrisani su i svi prateci podaci uz njega.");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden);
            }
        }


        [HttpGet]
        [Route("DeleteComment")]
        public HttpResponseMessage DeleteComment([FromUri] int ID)
        {
            User user = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(user, Functionalities.DELETE_DATA))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }
            Comment comment = commentsRepository.GetCommentByID(ID);
            if (commentsRepository.DeleteComment(ID))
            {
                eventsRepository.CalculateEventAverageGrade(comment.Event.ID);
                return Request.CreateResponse(HttpStatusCode.OK, "Komentar obrisan.");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden);
            }
        }


        [HttpGet]
        [Route("BlockUser")]
        public HttpResponseMessage BlockUser([FromUri] int ID)
        {
            User user = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(user, Functionalities.BLOCK_USER))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }
            if (usersRepository.BlockUserByID(ID))
            {
                return Request.CreateResponse(HttpStatusCode.OK, "Korisnik blokiran. Ovim nisu obrisani njegovi prateci podaci, manifestacije, karte..");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden);
            }
        }

        [HttpGet]
        [Route("DeleteTicket")]
        public HttpResponseMessage DeleteTicket([FromUri] int ID)
        {
            User user = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(user, Functionalities.DELETE_DATA))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }
            if (ticketsRepo.DeleteTicketByID(ID))
            {
                return Request.CreateResponse(HttpStatusCode.OK, "Karta obrisana");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden);
            }
        }


        [HttpGet]
        [Route("ApproveEvent")]
        public HttpResponseMessage ApproveEvent([FromUri] int ID)
        {
            User user = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(user, Functionalities.ACTIVATE_EVENT))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }
            if (eventsRepository.ApproveEventByID(ID))
            {
                return Request.CreateResponse(HttpStatusCode.OK, "Dogadjaj aktiviran");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden);
            }
        }


        [HttpGet]
        [Route("Search")]
        public HttpResponseMessage Search([FromUri] UserSearchModel model)
        {
            User user = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(user, Functionalities.VIEW_ALL_SYSTEM_USERS))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }

            List<User> allUsers = usersRepository.GetAllUsers();

            //Search name
            if (!string.IsNullOrEmpty(model.Name))
            {
                allUsers = allUsers.FindAll(x => x.Name == model.Name);
            }
            //Search last name
            if (!string.IsNullOrEmpty(model.Lastname))
            {
                allUsers = allUsers.FindAll(x => x.LastName == model.Lastname);
            }
            //Search username
            if (!string.IsNullOrEmpty(model.Username))
            {
                allUsers = allUsers.FindAll(x => x.Username == model.Username);
            }


            return Request.CreateResponse(HttpStatusCode.OK, allUsers);
        }
    }
}
