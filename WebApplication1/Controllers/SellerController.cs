﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using WebApplication1.Models.BindingModels;
using WebApplication1.Repositories;

namespace WebApplication1.Controllers
{
    [RoutePrefix("api/Seller")]
    public class SellerController : ApiController
    {
        TicketsRepository ticketsRepo = new TicketsRepository();
        UsersRepository usersRepository = new UsersRepository();
        EventsRepository eventsRepository = new EventsRepository();
        CommentsRepository commentsRepository = new CommentsRepository();
        LocationRepository locationRepository = new LocationRepository();
        AddressRepository addressRepository = new AddressRepository();
        IAccessRights accessRights = new AccessRightsResolver();
        

        [HttpGet]
        [Route("GetSellerEvents")]
        public HttpResponseMessage GetSellerEvents([FromUri] int ID)
        {
            User principal = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(principal, Functionalities.SELLER_EVENTS))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }
            return Request.CreateResponse(HttpStatusCode.OK,  eventsRepository.GetEventsBySellerID(ID).OrderBy(x => x.EventDate).ToList());
        }


        [HttpGet]
        [Route("GetSellerTickets")]
        public HttpResponseMessage GetSellerTickets([FromUri] int ID)
        {
            User principal = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(principal, Functionalities.VIEW_EVENT_RESERVED_TICKETS))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }
            return Request.CreateResponse(HttpStatusCode.OK, ticketsRepo.GetValidTicketsBySellerID(ID).OrderBy(x => x.Event.EventDate).ToList());
        }

        [HttpGet]
        [Route("GetSellerBuyers")]
        public HttpResponseMessage GetSellerBuyers([FromUri] int ID)
        {
            User principal = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(principal, Functionalities.VIEW_OWN_BUYERS))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }
            Dictionary<int, User> users = new Dictionary<int, User>();
            List<Ticket> allTickets =  ticketsRepo.GetValidTicketsBySellerID(ID);

            foreach(Ticket ticket in allTickets)
            {
                if(!users.ContainsKey(ticket.BuyerID))
                {
                    users.Add(ticket.BuyerID, usersRepository.GetUserByID(ticket.BuyerID));
                }
            }

            return Request.CreateResponse(HttpStatusCode.OK, users.Values.ToList());
        }


        [HttpGet]
        [Route("ApproveComment")]
        public HttpResponseMessage ApproveComment([FromUri] int ID)
        {
            User principal = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(principal, Functionalities.APPROVE_COMMENT))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }
            if (commentsRepository.ApproveComment(ID))
            {
                Comment comment = commentsRepository.GetCommentByID(ID);
                eventsRepository.CalculateEventAverageGrade(comment.Event.ID);
                return Request.CreateResponse(HttpStatusCode.OK, "Komentar odobren. Sada ce biti vidljiv i ostalima.");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden);
            }
        }


        [HttpGet]
        [Route("DenyComment")]
        public HttpResponseMessage DenyComment([FromUri] int ID)
        {
            User principal = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(principal, Functionalities.DENY_COMMENT))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }
            if (commentsRepository.DenyComment(ID))
            {
                return Request.CreateResponse(HttpStatusCode.OK, "Komentar odbijen.");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden);
            }
        }

        [HttpPost]
        [Route("UploadImage")]
        public HttpResponseMessage Post()
        {
            User principal = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(principal, Functionalities.ADD_EVENT))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }
            var fileName = HttpContext.Current.Request.Params["filename"];
            var result = new HttpResponseMessage(HttpStatusCode.OK);
            if (Request.Content.IsMimeMultipartContent())
            {
                //For larger files, this might need to be added:
                Request.Content.LoadIntoBufferAsync().Wait();
                Request.Content.ReadAsMultipartAsync<MultipartMemoryStreamProvider>(
                        new MultipartMemoryStreamProvider()).ContinueWith((task) =>
                        {
                            MultipartMemoryStreamProvider provider = task.Result;
                            int i = 0;
                            foreach (HttpContent content in provider.Contents)
                            {
                                if (i > 0) break;
                                Stream stream = content.ReadAsStreamAsync().Result;
                                Image image = Image.FromStream(stream);
                                var testName = content.Headers.ContentDisposition.Name;
                                String filePath = HostingEnvironment.MapPath("~/Images/");
                                String fullPath = Path.Combine(filePath, fileName);
                                image.Save(fullPath);
                                i++;
                            }
                        });
                return result;
            }
            else
            {
                throw new HttpResponseException(Request.CreateResponse(
                        HttpStatusCode.NotAcceptable,
                        "This request is not properly formatted"));
            }
        }


        [HttpPost]
        [Route("CreateEvent")]
        public HttpResponseMessage CreateEvent([FromBody] EventBindingModel model)
        {
            User principal = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(principal, Functionalities.ADD_EVENT))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }
            Seller seller = (Seller)principal;

            if(!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Uneseni podaci su nevalidni!");
            }

            DateTime eventDate = new DateTime(model.EventYear, model.EventMonth, model.EventDay, model.EventHour, model.EventMinutes, model.EventSeconds);

            if((eventDate - DateTime.Now ).Days <= 30)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Dogadjaj ne moze biti kreiran manje od 30 dana pre desavanja.");

            }

            if (model.SeatsNumber != (model.RegularTicketsNumber + model.FanPitTicketsNumber + model.VipTicketsNumber))
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Broj karata se ne poklapa sa brojem mesta.");

            }
            Address address = new Address(model.EventCity, model.EventStreetNumber, model.EventStreet, model.EventCityZip);
            if(eventsRepository.DoesEventExistOnCoordinatesAndTime(eventDate, address))
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Vec postoji dogadjaj na ovoj adresi u istom danu, nije moguce kreirati jos jedan.");
            }

            
            int addressID = addressRepository.CreateAddress(address);
            address = addressRepository.GetAddressByID(addressID);
            Location location = new Location(model.EventGeographicLength, model.EventGeographicWidth, address);
            locationRepository.CreateLocation(location);

            Event newEvent = new Event(address, model.FileName, eventDate, model.EventType, model.EventName, model.SeatsNumber, model.TicketPrice, seller,
                                        model.RegularTicketsNumber, model.FanPitTicketsNumber, model.VipTicketsNumber);
            eventsRepository.CreateEvent(newEvent);

            return Request.CreateResponse(HttpStatusCode.OK, "Dogadjaj kreiran. Bice vidljiv kada ga administrator aktivira.");
        }


        [HttpPost]
        [Route("EditEvent")]
        public HttpResponseMessage EditEvent([FromBody] EditEventBindingModel model)
        {
            User principal = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(principal, Functionalities.EDIT_EVENT))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }
            Seller seller = (Seller)principal;
            
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Uneseni podaci su nevalidni!");
            }
            Event oldEvent = eventsRepository.GetEventByID(model.ID);
            List<Ticket> allValidEventTickets = ticketsRepo.GetValidTicketsByEventID(model.ID);

            DateTime eventDate = new DateTime(model.EventYear, model.EventMonth, model.EventDay, model.EventHour, model.EventMinutes, model.EventSeconds);

            if ((eventDate - DateTime.Now).Days <= 7)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Ne mozete pomeriti dogadjaj na manje od nedelju dana od trenutnog datuma.");

            }

            if (model.SeatsNumber < (model.RegularTicketsNumber + model.FanPitTicketsNumber + model.VipTicketsNumber + allValidEventTickets.Count))
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Broj mesta ne moze biti manji od ukupnog broja prodatih i neprodatih karata na dogadjaju.");

            }
            Address address = new Address(model.EventCity, model.EventStreetNumber, model.EventStreet, model.EventCityZip);
            int addressID = addressRepository.CreateAddress(address);
            if (eventsRepository.DoesEventExistOnCoordinatesAndTime(eventDate, address) && addressID != oldEvent.EventAddress.ID)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Vec postoji dogadjaj na ovoj adresi u istom danu, nije moguce kreirati jos jedan.");
            }
            string filename = oldEvent.EventCover;
            if(!(model.FileName == null))
            {
                filename = model.FileName;
            }

            address = addressRepository.GetAddressByID(addressID);
            Location oldLocation = locationRepository.GetLocationByAddressID(oldEvent.EventAddress.ID);
            if(oldLocation.GeographicLength != model.EventGeographicLength || oldLocation.GeographicWidth != model.EventGeographicWidth)
            {
                Location location = new Location(model.EventGeographicLength, model.EventGeographicWidth, address);
                locationRepository.CreateLocation(location);
            }
            

            Event newEvent = new Event(oldEvent.IsActive,filename, eventDate, model.EventType, model.EventName, model.SeatsNumber, model.TicketPrice, address, seller, model.RegularTicketsNumber,
                                        model.FanPitTicketsNumber, model.VipTicketsNumber, oldEvent.AverageGrade, oldEvent.ID, oldEvent.IsDeleted()) ;
            eventsRepository.UpdateEvent(newEvent);
            ticketsRepo.UpdateTicketsEventData(newEvent.ID, newEvent);

            return Request.CreateResponse(HttpStatusCode.OK, "Dogadjaj izmenjen.");
        }
    }
}
