﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebApplication1.Models.BindingModels;
using WebApplication1.Repositories;

namespace WebApplication1.Controllers
{
    [RoutePrefix("api/Buyer")]
    public class BuyerController : ApiController
    {
        TicketsRepository ticketsRepo = new TicketsRepository();
        UsersRepository usersRepository = new UsersRepository();
        CommentsRepository commentsRepository = new CommentsRepository();
        EventsRepository eventsRepository = new EventsRepository();
        IAccessRights accessRights = new AccessRightsResolver();

        [HttpGet]
        [Route("GetTickets")]
        public HttpResponseMessage GetTickets([FromUri] int ID)
        {
            User principal = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(principal, Functionalities.VIEW_OWN_RESERVED_TICKETS))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }
            return Request.CreateResponse(HttpStatusCode.OK, ticketsRepo.GetValidTicketsByBuyerID(ID).OrderBy(x => x.Event.EventDate).ToList());
        }


        [HttpGet]
        [Route("Search")]
        public HttpResponseMessage Search([FromUri] HomeSearchModel model)
        {
            User principal = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(principal, Functionalities.VIEW_OWN_RESERVED_TICKETS))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }

            //Add authorization here

            List<Ticket> allTickets = ticketsRepo.GetValidTicketsByBuyerID(principal.ID).OrderBy(x => x.Event.EventDate).ToList();

            DateTime dateFrom = new DateTime();
            if (model.DayFrom != -1 && model.MonthFrom != -1 && model.YearFrom != -1) dateFrom = new DateTime(model.YearFrom, model.MonthFrom, model.DayFrom);

            DateTime dateTo = new DateTime();
            if (model.DayTo != -1 && model.MonthTo != -1 && model.YearTo != -1) dateTo = new DateTime(model.YearTo, model.MonthTo, model.DayTo);



            //Search event name
            if (!string.IsNullOrEmpty(model.EventName))
            {
                allTickets = allTickets.FindAll(x => x.Event.Name == model.EventName);
            }
            //Search price from
            if (model.PriceFrom != -1)
            {
                allTickets = allTickets.FindAll(x => x.PaidPrice >= model.PriceFrom);
            }
            //Search price to
            if (model.PriceTo != -1)
            {
                allTickets = allTickets.FindAll(x => x.PaidPrice <= model.PriceTo);
            }
            //Search date from
            if (dateFrom != default(DateTime))
            {
                allTickets = allTickets.FindAll(x => x.Event.EventDate.CompareTo(dateFrom) >= 0);
            }

            //Search date from
            if (dateTo != default(DateTime))
            {
                allTickets = allTickets.FindAll(x => x.Event.EventDate.CompareTo(dateTo) <= 0);
            }

            allTickets = allTickets.OrderBy(x => x.Event.EventDate).ToList();
            return Request.CreateResponse(HttpStatusCode.OK, allTickets);
        }

        [HttpGet]
        [Route("CancellTicket")]
        public HttpResponseMessage CancellTicket([FromUri] int ID)
        {
            User principal = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(principal, Functionalities.CANCEL_TICKET))
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }
            Buyer user = (Buyer)principal;


            if (ticketsRepo.CancellTicket(ID))
            {
                Ticket ticket = ticketsRepo.GetTicketByID(ID);
                int newPoints = -(int)(ticket.PaidPrice / 1000 * 133 * 4);
                usersRepository.UpdateBuyerPoints(user.ID, newPoints);
                usersRepository.EvaluateSuspiciousness(user.ID);
                return Request.CreateResponse(HttpStatusCode.OK, "Ticket Cancelled");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Ticket ID does not exist");
            }

        }


        [HttpGet]
        [Route("UserHasReservedTicketsOnEvent")]
        public HttpResponseMessage UserHasReservedTicketsOnEvent([FromUri] int ID)
        {
            //No authorization required
            User principal = (User)HttpContext.Current.Session["user"];
            if(principal == null) return Request.CreateResponse(HttpStatusCode.OK, false);

            return Request.CreateResponse(HttpStatusCode.OK, ticketsRepo.DoesUserHaveReservedTickets(principal.ID, ID));

        }


        [HttpPost]
        [Route("PostComment")]
        public HttpResponseMessage PostComment([FromBody] CommentBindingModel commentModel)
        {
            User user = usersRepository.GetUserByID(commentModel.CurrentUser);
            User principal = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(principal, Functionalities.COMMENT) || user.ID != principal.ID)
            {
                
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }
            

            if(!ModelState.IsValid) Request.CreateResponse(HttpStatusCode.Forbidden, "Error occured creating a comment");

            Event _event = eventsRepository.GetEventByID(commentModel.CurrentEvent);
            if(_event.EventDate > DateTime.Now) Request.CreateResponse(HttpStatusCode.Forbidden, "You are not allowed to comment yet.");

            Comment comment = new Comment(commentModel.IsApproved, commentModel.Grade, commentModel.Text, user, _event, commentModel.IsDenied);

            if (commentsRepository.CreateComment(comment))
            {
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, false);
            }

        }


        [HttpPost]
        [Route("ReserveTickets")]
        public HttpResponseMessage ReserveTickets([FromBody] ReservationBindingModel ticketModel)
        {
            Buyer user = (Buyer)usersRepository.GetUserByID(ticketModel.CurrentUser);
            User principal = (User)HttpContext.Current.Session["user"];
            if (!accessRights.CanView(principal, Functionalities.RESERVE_TICKET) || user.ID != principal.ID)
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You  are not allowed to use this.");
            }

            if (!ModelState.IsValid) Request.CreateResponse(HttpStatusCode.Forbidden, "Error occured creating a reservation");

            Event _event = eventsRepository.GetEventByID(ticketModel.CurrentEvent);
            if (_event.EventDate < DateTime.Now) Request.CreateResponse(HttpStatusCode.Forbidden, "You are not allowed reserve tickets anymore.");
            if(!_event.IsActive) Request.CreateResponse(HttpStatusCode.Forbidden, "You are not allowed reserve tickets on an inactive event.");

            if (_event.RegularTickets < ticketModel.RegularTickets) Request.CreateResponse(HttpStatusCode.Forbidden, "Not enough regular tickets available.");
            if (_event.FanPitTickets < ticketModel.FanPitTickets) Request.CreateResponse(HttpStatusCode.Forbidden, "Not enough fan pit tickets available.");
            if (_event.VipTickets < ticketModel.VipTickets) Request.CreateResponse(HttpStatusCode.Forbidden, "Not enough vip tickets available.");

            string buyerName = user.Name + " " + user.LastName;
            double discountedPrice = user.CalculateDiscountedPrice(_event.TicketBasePrice);
            double discountedPriceFan = user.CalculateDiscountedPrice(_event.TicketBasePrice * 2);
            double discountedPriceVip = user.CalculateDiscountedPrice(_event.TicketBasePrice * 4);
            int pointsEarned = (int)(_event.TicketBasePrice * 133 * ticketModel.RegularTickets / 1000);
            pointsEarned += (int)(_event.TicketBasePrice * 133 * ticketModel.FanPitTickets * 2 / 1000);
            pointsEarned += (int)(_event.TicketBasePrice * 133 * ticketModel.VipTickets * 4 / 1000);

            for (int i = 0; i < ticketModel.RegularTickets; i++)
            {
                Ticket ticket = new Ticket(buyerName, _event, "", TicketType.REGULAR, true, user.ID, discountedPrice);
                while (true)
                {
                    string ticketID = IDRepository.GetHashString(user.Username + _event.Name + DateTime.Now).Substring(0, 10);
                    ticket.TicketID = ticketID;
                    if (ticketsRepo.CreateReservation(ticket, false)) break;
                }
            }
            for (int i = 0; i < ticketModel.FanPitTickets; i++)
            {
                Ticket ticket = new Ticket(buyerName, _event, "", TicketType.FAN_PIT, true, user.ID, discountedPriceFan);
                while (true)
                {
                    string ticketID = IDRepository.GetHashString(user.Username + _event.Name + DateTime.Now).Substring(0, 10);
                    ticket.TicketID = ticketID;
                    if (ticketsRepo.CreateReservation(ticket, false)) break;
                }
                
            }
            for (int i = 0; i < ticketModel.VipTickets; i++)
            {
                Ticket ticket = new Ticket(buyerName, _event, "", TicketType.VIP, true, user.ID, discountedPriceVip);
                while (true)
                {
                    string ticketID = IDRepository.GetHashString(user.Username + _event.Name + DateTime.Now).Substring(0, 10);
                    ticket.TicketID = ticketID;
                    
                    if (ticketsRepo.CreateReservation(ticket, false)) break;
                }
            }

            eventsRepository.ChangeEventTicketsNumber(_event.ID, -ticketModel.RegularTickets, -ticketModel.FanPitTickets, -ticketModel.VipTickets);
            usersRepository.UpdateBuyerPoints(user.ID, pointsEarned);
            ticketsRepo.SerializeAllTickets();
            return Request.CreateResponse(HttpStatusCode.OK, "Tickets reserved successfully");
        }

    }
}
