﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Models.BindingModels;
using WebApplication1.Repositories;

namespace WebApplication1.Controllers
{
    [RoutePrefix("api/Home")]
    public class HomeController : ApiController
    {
        EventsRepository eventsRepo = new EventsRepository();
        CommentsRepository commentsRepository = new CommentsRepository();
        LocationRepository locationRepository = new LocationRepository();

        [HttpGet]
        [Route("GetAllEvents")]
        public List<Event> GetAllEvents()
        {
            //No authorization required here
            List<Event> allEvents = eventsRepo.GetAllActiveEvents();
            allEvents = allEvents.OrderBy(x => x.EventDate).ToList();
            return allEvents;
        }


        [HttpGet]
        [Route("GetEvent")]
        public Event GetEvent([FromUri] int ID)
        {
            //No authorization required here
            return eventsRepo.GetEventByID(ID);
        }

        [HttpGet]
        [Route("GetEventComments")]
        public List<Comment> GetEventComments([FromUri] int ID)
        {
            //No authorization required here
            return commentsRepository.GetValidCommentsByEventID(ID);
        }

        [HttpGet]
        [Route("GetEventLocation")]
        public Location GetEventLocationByAddress([FromUri] int ID)
        {
            //No authorization required here
            return locationRepository.GetLocationByAddressID(ID);
        }

        [HttpGet]
        [Route("Search")]
        public List<Event> Search([FromUri] HomeSearchModel model)
        {
            //No authorization required here
            List<Event> allEvents = eventsRepo.GetAllActiveEvents().OrderBy(x => x.EventDate).ToList();

            DateTime dateFrom = new DateTime();
            if (model.DayFrom != -1 && model.MonthFrom != -1 && model.YearFrom != -1) dateFrom = new DateTime(model.YearFrom, model.MonthFrom, model.DayFrom);

            DateTime dateTo = new DateTime();
            if (model.DayTo != -1 && model.MonthTo != -1 && model.YearTo != -1) dateTo = new DateTime(model.YearTo, model.MonthTo, model.DayTo);


            //Search by city
            if (!string.IsNullOrEmpty(model.EventCity))
            {
                allEvents = allEvents.FindAll(x => x.EventAddress.City == model.EventCity);
            }
            //Search event name
            if (!string.IsNullOrEmpty(model.EventName))
            {
                allEvents = allEvents.FindAll(x => x.Name == model.EventName);
            }
            //Search street
            if (!string.IsNullOrEmpty(model.EventStreet))
            {
                allEvents = allEvents.FindAll(x => x.EventAddress.Street == model.EventStreet);
            }
            //Search price from
            if (model.PriceFrom != -1)
            {
                allEvents = allEvents.FindAll(x => x.TicketBasePrice >= model.PriceFrom);
            }
            //Search price to
            if (model.PriceTo != -1)
            {
                allEvents = allEvents.FindAll(x => x.TicketBasePrice <= model.PriceTo);
            }
            //Search date from
            if (dateFrom != default(DateTime))
            {
                allEvents = allEvents.FindAll(x => x.EventDate.CompareTo(dateFrom) >= 0);
            }

            //Search date from
            if (dateTo != default(DateTime))
            {
                allEvents = allEvents.FindAll(x => x.EventDate.CompareTo(dateTo) <= 0);
            }

            allEvents = allEvents.OrderBy(x => x.EventDate).ToList();
            return allEvents;
        }

    }
}
